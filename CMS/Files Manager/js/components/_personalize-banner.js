﻿import getVariables from './_variables.js'
import { stringToSlug, formatMoney } from './_functions.js'


const headers = getVariables('headers');
export default class personalizeBanner {
    constructor(element = '[data-admake="fileuploader"]') {
        if( $('.product-main-personalize-banner').length ){
            let self = this;
            let aux = $(element).attr('data-element');

            self._$el = $(element);
            self._store = $(element).attr('data-store');
            self._sigla = $(element).attr('data-sigla');
            self._$form = $(element).find(aux);
            
            self.events();
        }
    }
    createAction() {

    }
    events() {
        const self = this;
        
        $("input[type=file].file").on('change',function(){
            // alert(this.files[0].name);
            $(this).parents('.box-upload').addClass('uploaded');
        });
        
        $('.box-upload.checagem-profissional').on('click', function(){
            $('#modal-checagem-profissional').fadeIn();
        }); 

        $('#modal-checagem-profissional .mask, #modal-checagem-profissional .close').on('click', function(){
            $('#modal-checagem-profissional').hide();
        });

    }
}

