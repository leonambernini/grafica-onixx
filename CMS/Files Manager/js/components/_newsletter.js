import getVariables from './_variables.js'
const _variables = getVariables();

export default class Newsletter {
    constructor(element = '[data-admake="newsletter"]') {
        let aux = $(element).attr('data-element');

        this._$el = $(element);
        this._store = $(element).attr('data-store');
        this._sigla = $(element).attr('data-sigla');
        this._$form = $(element).find(aux)

        if (this._$el.length) {
            if (this._store == undefined || this._store == null || this._store == '') {
                console.log('Store nгo informada');
                this._$el.remove();
            } else if (this._sigla == undefined || this._sigla == null || this._sigla == '') {
                console.log('Sigla nгo informada');
                this._$el.remove();
            } else if (!this._$form.length) {
                console.log('Formulario invбlido');
                this._$el.remove();
            } else {
                this.createAction();
                this.events();
            }
        }
    }
    createAction() {
        // const url = "//api.vtexcrm.com.br/" + this._store + "/dataentities/" + this._store + "/documents/";
        const url = "/api/dataentities/" + this._store + "/documents/";
        this._$form.attr('action', url);
    }
    events() {
        const self = this;

        this._$el.find('form').on('submit', function (e) {
            e.preventDefault();
            
            let $this = $(this),
                $name = $this.find('.name'),
                $email = $this.find('.email'),
                $btn = $(this).find('button.btn-primary'),
                $alert = $(this).find('.alert'),
                $aux = $btn.html(),
                auxName;

                if(screen.width < 768){
                    $btn = $(this).find('button.btn-mobile');
                    $aux = $btn.html();
                }

            // const urlSearchEmail = `//api.vtex.com/${self._store}/dataentities/${self._sigla}/search?_where=email=${$email.val()}`,
            //     urlDocument = `//api.vtex.com/${self._store}/dataentities/${self._sigla}/documents`;
            const urlSearchEmail = `/api/dataentities/${self._sigla}/search?_where=email=${$email.val()}`,
                urlDocument = `/api/dataentities/${self._sigla}/documents`;
            
                // alert(urlSearchEmail);

            if ($email.val()) {
                $.ajax({
                    url: urlSearchEmail,
                    type: 'GET',
                    // beforeSend: () => ($btn.html(HTML_BTN_LOADER)),
                    beforeSend: () => ($btn.html(_variables.loader)),
                    headers: _variables.headers
                })
                .done((data) => {
                    if (data.length) {
                        $alert.html('Email já cadastrado na base').show().css('display', 'block').delay(5000).fadeOut();
                    } else {
                        auxName = $name.val() ? $name.val() : '';

                        $.ajax({
                            url: urlDocument,
                            type: 'PATCH',
                            headers: _variables.headers,
                            data: JSON.stringify({ email: $email.val(), name: auxName }),
                        })
                        .done((data) => {
                            $alert.html('Email cadastrado com sucesso !').show().css('display', 'block').delay(5000).fadeOut();
                            $email.val('')
                            $name.val('')
                        })
                        .fail(function () {
                            console.log("error");
                        })
                    }
                })
                .fail(function () {
                    $alert.html('Erro na requisiзгo');
                })
                .complete(function () {
                    $btn.html($aux);
                })
            } else {
                $alert.html('E-mail invбlido').show().css('display', 'block').delay(5000).fadeOut();
            }

        });
    }
}



