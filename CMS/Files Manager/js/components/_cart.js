import getVariables from './_variables.js'
import { formatMoneyCheckout, getParameter } from './_functions.js'

const _variables = getVariables();

export default class Cart {
    constructor(parent = 'body', element = '[data-admake="cart-sidebar"]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;
        this._$parent = $(parent);

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.cart, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.cart };
            }

            this.createStruct();
            this.createListItems();
            this.events();
        }
    }
    createStruct() {
        const current = this._config;

        let htmlMain = `<div id="${current.elementMain}" style="display: none;">
                            <div class="mask"></div>
                            <div class="content">
                                <div class="cart-header d-flex justify-content-between align-items-center">
                                    <svg width="21px" height="23px" viewBox="0 0 21 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="carrinho-lateral" transform="translate(-992.000000, -25.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                                <g id="Group-8" transform="translate(992.000000, 25.000000)">
                                                    <path d="M9.625,20.6329685 C9.625,21.7720558 8.7015873,22.6954685 7.5625,22.6954685 C6.4234127,22.6954685 5.5,21.7720558 5.5,20.6329685 C5.5,19.4938812 6.4234127,18.5704685 7.5625,18.5704685 C8.7015873,18.5704685 9.625,19.4938812 9.625,20.6329685 Z M16.5,20.6329685 C16.5,19.4938812 15.5765873,18.5704685 14.4375,18.5704685 C13.2984127,18.5704685 12.375,19.4938812 12.375,20.6329685 C12.375,21.7720558 13.2984127,22.6954685 14.4375,22.6954685 C15.5765873,22.6954685 16.5,21.7720558 16.5,20.6329685 Z M7.28306304e-14,2.07046847 C7.28306304e-14,1.31107694 0.615608469,0.695468469 1.375,0.695468469 L3.4375,0.695468469 C3.97408787,0.696100163 4.46128728,1.00882045 4.6853125,1.49640597 L5.5825,3.44546847 L19.25,3.44546847 C19.7360887,3.44590388 20.1858264,3.70295745 20.4328999,4.12157068 C20.6799735,4.54018392 20.6876631,5.05814279 20.453125,5.48390597 L17.015625,11.671406 C16.7740459,12.1099518 16.3131822,12.3825198 15.8125,12.3829685 L7.21875,12.3829685 C6.64920635,12.3829685 6.1875,12.8446748 6.1875,13.4142185 C6.1875,13.9837621 6.64920635,14.4454685 7.21875,14.4454685 L17.1875,14.4454685 C17.9468915,14.4454685 18.5625,15.0610769 18.5625,15.8204685 C18.5625,16.57986 17.9468915,17.1954685 17.1875,17.1954685 L7.21875,17.1954685 C5.45776058,17.1915092 3.93262968,15.9724341 3.54079422,14.2555869 C3.14895875,12.5387397 3.99416357,10.7786837 5.5790625,10.0110935 L2.5575,3.44546847 L1.375,3.44546847 C0.615608469,3.44546847 7.28306304e-14,2.82986 7.28306304e-14,2.07046847 Z M6.8475,6.19546847 L8.42875,9.63296847 L15.0046875,9.63296847 L16.9125,6.19546847 L6.8475,6.19546847 Z" id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <strong class="flex-grow-1">${current.title}</strong>
                                    <button class="btn cart-btn-close ml-3"><i class="fa fa-times"></i></button>
                                </div>
                                <div class="cart-body">
                                    <p class="cart-loader">
                                        ${current.loader}
                                        <span class="sr-only">Loading...</span>
                                    </p>
                                    <div class="cart-items"></div>
                                </div>
                                <div class="cart-footer">`;

        // if (current.alerts) {
        //     htmlMain += '          <ul class="cart-alerts list-unstyled">';
        //     current.alerts.forEach(element => {
        //         htmlMain += '           <li>' + element + '</li>';
        //     });
        //     htmlMain += '         </ul>';
        // }

        htmlMain += '             <div class="cart-values">';

        if (current.subTotal) {
            htmlMain += `           <div class="cart-subtotal d-flex justify-content-between align-items-center" style="display: none;">
                                        <span>Valor:</span>
                                        <strong>R$ 0,00</strong>
                                    </div>`;
        }

        if (current.discount) {
            htmlMain += `           <div class="cart-discont d-flex justify-content-between align-items-center" style="display: none;">
                                        <span>Descontos</span>
                                        <strong>-</strong>
                                    </div>`
        }

        if (current.total) {
            htmlMain += `           <div class="cart-total d-flex justify-content-between align-items-center">
                                        <span>Total</span>
                                        <strong>-</strong>
                                    </div>`;
        }

        htmlMain += '           </div>';

        htmlMain += '		        <div class="cart-actions">';
        htmlMain += '				    <div>';

        if (current.keepBuying !== null && current.keepBuying !== '') {
            htmlMain += '				<a class="btn cart-btn-keep-buying cart-btn-close">' + current.keepBuying + '</a>';
        }

        if (current.btnBuyMore !== null && current.btnBuyMore !== '') {
            htmlMain += '				<a class="btn cart-btn-buymore cart-btn-close">' + current.btnBuyMore + '</a>';
        }
        if (current.btnGoCart !== null && current.btnGoCart !== '') {
            htmlMain += '				<a class="btn cart-btn-buy" href="/checkout/#/cart">' + current.btnGoCart + '</a>';
        }

        htmlMain += `                   </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

        this._$parent.append(htmlMain);
    }
    createListItems(flag = null) {
        const current = this._config, $cart = $(`#${current.elementMain}`);
        let htmlItems = '';

        $cart.find('.cart-loader').show();

        vtexjs.checkout.getOrderForm().done((orderForm) => {

            if (orderForm.items.length) {
                orderForm.items.forEach((element, index) => {
                    htmlItems += `<div class="item d-flex align-items-center justify-content-between" data-sku="${element.id}" data-ref="${element.refId}" data-prod="${element.productId}">
                                        <div class="item-image">
                                            <a href="${element.detailUrl}" title="${element.name}">
                                                <img class="img-fluid" src="${element.imageUrl}"/>
                                            </a>
                                        </div>
                                        <div class="item-details">
                                            <div class="item-name">
                                                <a href="${element.detailUrl}" title="${element.name}">${element.name}</a>
                                            </div>
                                            <div class="item-price-actions">`;
                    if (current.qtyItem) {
                        htmlItems += `<span class="item-qty">${element.quantity}x</span>`;
                    }
                    if (current.listPrice) {
                        htmlItems += `<span class="item-listprice">R$ ${formatMoneyCheckout(element.listPrice, 2, ',', '.')}</span>`;
                    }
                    htmlItems += `  <span class="item-bestprice">R$ ${formatMoneyCheckout(element.price, 2, ',', '.')}</span>`;

                    if(element.sellingPrice <= 0){
                        htmlItems += `<span class="item-freeprice">(Gratuito)</span>`;
                    }
                    htmlItems += `</div></div>
                        <button class="item-remove btn" index="${index}">
                            ${current.btnRemove}
                        </button>
                    </div>`;

                });
            } else {
                htmlItems += `<button class="cart-btn-close">Carrinho vazio</button>`;
            }

            if (current.selectorBadge !== null && $(current.selectorBadge).length) {
                $(current.selectorBadge).html(orderForm.items.length);
            }

            if (current.subTotal && orderForm.value !== 0) {
                if (orderForm.totalizers[0]) {
                    $cart.find('.cart-subtotal > strong').html('+ R$ ' + formatMoneyCheckout(orderForm.totalizers[0].value, 2, ',', '.'));
                    $cart.find('.cart-subtotal').show();
                } else {
                    $cart.find('.cart-subtotal').hide();
                }
            }

            if (current.discount && orderForm.value !== 0) {
                if (orderForm.totalizers[1]) {
                    $cart.find('.cart-discount > strong').html('R$ ' + formatMoneyCheckout(orderForm.totalizers[1].value, 2, ',', '.'));
                    $cart.find('.cart-discount').show();
                } else {
                    $cart.find('.cart-discount').hide();
                }
            }

            $cart.find('.cart-total > strong').html('R$ ' + formatMoneyCheckout(orderForm.value, 2, ',', '.'));
            $cart.find('.cart-items').html(htmlItems);
            $cart.find('.cart-loader').hide();

            if (flag == 'open') {
                this.cartOpen();
            }
        });
    }
    removeItem(index) {
        const self = this;

        vtexjs.checkout.getOrderForm().then(function (orderForm) {
            let item = orderForm.items[index];

            item.index = index;
            return vtexjs.checkout.removeItems([item]);

        }).done(function (orderForm) {
            self.createListItems();

        });
    }
    cartClose() {
        const current = this._config;

        $(`#${current.elementMain} .content`).animate({
            right: '-100%'
        }, 300, function () {
            $(`#${current.elementMain}`).fadeOut(100);
        });
    }
    cartOpen() {
        const current = this._config;

        $(`#${current.elementMain}`).fadeIn(100, function () {
            $(`#${current.elementMain} .content`).animate({
                right: 0
            }, 300);
        });

        $('.cart-body').css('max-height', `calc(100% - ${$('.cart-footer').outerHeight() + 60}px`);

    }
    events() {
        const current = this._config,
            self = this;

        $(document).on('click', `#${current.elementMain} .mask, #${current.elementMain} .cart-btn-close`, function (e) {
            e.preventDefault();
            self.cartClose();
        });

        $(document).on('click', this._element, function (e) {
            e.preventDefault();
            self.cartOpen();
        });

        $(document).on('click', '.item-remove', function (e) {
            e.preventDefault();
            self.removeItem($(this).attr('index'));
        });

        if (current.buyAsyn) {
            $(document).on('click', current.elementBuy, function (e) {
                const $this = $(this);
                let urlButton = $this.attr('href'),
                    htmlDefault = $this.html(),

                    inputQty = $(current.elementQty).attr('element'),
                    parentQty = $(current.elementQty).attr('parent'),

                    $parents = $this.parents(parentQty),
                    qty = parseInt($parents.find(inputQty).val());

                    if(parentQty == '#ad-product-buy-button-price'){
                        qty = parseInt($('#ad-product-buy-button-price').find('.quantitySelector > input').val());
                    };

                if($('#admake-personalizar-porta').length){
                    //Vai adicionar os anexos ao carrinho
                    if (urlButton.indexOf('cart') >= 0) {
                        e.preventDefault();
                        
                        if (qty < 1 || !qty) {
                            qty = 1;
                        }

                        urlButton = urlButton.replace(`qty=${getParameter('qty', urlButton)}`, `qty=${qty}`);
                        $this.attr('href', urlButton);

                        var attachment_qtde = $('.attachment-box').length;
                        var attachment_cont = 0;
                        $.each($('.attachment-box'), function () {
                            if($(this).find('input.attachment-value').val() == ""){
                                alert($(this).find('input.attachment-value').attr('data-message'));
                                return false;
                            }else{
                                attachment_cont++
                            }
                        });

                        if(attachment_qtde == attachment_cont){
                            $.ajax({
                                url: urlButton,
                                beforeSend: function () {
                                    $this.html(current.loader);
                                },
                                success: function () {
                                    vtexjs.checkout.getOrderForm().done(function (orderForm) {
                                        var itemIndex = orderForm.items.length - 1 || 0;
                                        
                                        $.each($('.attachment-box'), function () {
                                            let attachmentValue = $(this).find('input.attachment-value').val();
                                            let attachmentName = $(this).find('input.attachment-value').attr('data-id');
                                            
                                            let content = {};
                                            content[attachmentName] = attachmentValue;
    
                                            vtexjs.checkout.addItemAttachment(itemIndex, attachmentName, content).done(function (orderForm) {});
                                                                                    
                                        });
    
                                        // var attachmentName = 'Nome';
                                        // var attachmentValue = $('.personalize-options .personalize-input input[name="personalize-name"]').val();
                                        // var content = {
                                        //     Nome: attachmentValue
                                        // };
                                        
                                        // vtexjs.checkout.addItemAttachment(itemIndex, attachmentName, content)
                                        //     .done(function (orderForm) {
                                        //         console.log('Produto Adicionado')
                                        //     });
    
                                        //Atualizar e abrir Carrinho
                                        self.createListItems('open');
                                    });
                                    // self.createListItems('open');
                                },
                                complete: () => {
                                    $this.html(htmlDefault);
                                },
                                error: function () {
                                    console.log('error');
                                }
                            });
                        }
                    }


                } else if($('.product-main-personalize-banner').length){
                    //Vai adicionar os anexos ao carrinho
                    if (urlButton.indexOf('cart') >= 0) {
                        e.preventDefault();

                        if (qty < 1 || !qty) {
                            qty = 1;
                        }
                        urlButton = urlButton.replace(`qty=${getParameter('qty', urlButton)}`, `qty=${qty}`);
                        $this.attr('href', urlButton);
                        
                        const store = 'graficaonixx',
                            sigla = 'FU';

                        // const urlDocument = `//api.vtex.com/${store}/dataentities/${sigla}/documents`;
                        
                        const urlDocument = `/api/dataentities/${sigla}/documents`;
                        
                        if($('.attachment-format').val() == ""){
                            alert('Escolha o formato');
                        }else if (!$('input[type=file].file').val()) {
                            alert("É Obrigatório enviar um aquivo!" );
                        }else{
                            $.ajax({
                                url: urlDocument,
                                type: 'PATCH',
                                headers: _variables.headers,
                                data: JSON.stringify({ file: "" }),
                            }).done((data) => {
                                let document_id = data['Id'].split("FU-")[1];
                                
                                /* Iniciando o FormData para construir os valores e dados */
                                var dataForm = new FormData();
                                dataForm.append('file', $('input[type=file].file')[0].files[0]);

                                /* Agora o ajax para fazer o upload. */
                                $.ajax({
                                    url: `/api/dataentities/${sigla}/documents/${document_id}/file/attachments`,
                                    type: 'POST',
                                    data: dataForm,
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    enctype: "multipart/form-data",
                                    beforeSend: function (a) {
                                        a.setRequestHeader("Accept", "application/json");
                                    },
                                    success: function (data, textStatus, request) {},
                                    error: function (data, textStatus, request) {}
                                });
                                
                                $.ajax({
                                    url: urlButton,
                                    beforeSend: function () {
                                        $this.html(current.loader);
                                    },
                                    success: function () {
                                        setTimeout(function(){
                                            vtexjs.checkout.getOrderForm().done(function (orderForm) {
                                                var itemIndex = orderForm.items.length - 1 || 0;
                                                
                                                //Adicionando formato ao pedido
                                                var attachmentName = 'PersonalizacaoBanner';
                                                var attachmentFormatValue = $('.attachment-format').val();
                                                var content = {
                                                    Formato: attachmentFormatValue,
                                                    ArquivoBanner: document_id
                                                };
                                                
                                                vtexjs.checkout.addItemAttachment(itemIndex, attachmentName, content, null, false).done(function (orderForm) {});
                                                /* ************************** */
                                                
                                                // Atualizar e abrir Carrinho
                                                self.createListItems('open');
                                                
                                                // Adicionando Checagem profissional caso a mesma esteja selecionada
                                                setTimeout(function(){
                                                    if($this.parents('#ad-product-buy-button')){
                                                        if($('.admake-acessories .buy-product-checkbox:not(.buy-product-checkbox-checked)').is(':checked')){
                                                            let urlButtonChecagemProfissional = $('.admake-acessories .buy-button-normal > a').attr('href');
                                                            if (urlButtonChecagemProfissional.indexOf('cart') >= 0) {
                                                                e.preventDefault();
                                                                                                                        
                                                                $.ajax({
                                                                    url: urlButtonChecagemProfissional,
                                                                    beforeSend: function () {},
                                                                    success: function () {
                                                                        // Atualizar e abrir Carrinho
                                                                        self.createListItems('open');
                                                                    },
                                                                    complete: () => {
                                                                        $this.html(htmlDefault);
                                                                    },
                                                                    error: function () {
                                                                        console.log('error');
                                                                    }
                                                                });
                                                            }
                                                        };
                                                    };
                                                },500);

                                            });
                                        },500);
                                    },
                                    complete: () => {
                                        $this.html(htmlDefault);
                                    },
                                    error: function () {
                                        console.log('error');
                                    }
                                });

                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                console.log("error");
                            })
                        }
                    }
                }else{
                    if (urlButton.indexOf('cart') >= 0) {
                        e.preventDefault();
                        
                        if (qty < 1 || !qty) {
                            qty = 1;
                        }

                        urlButton = urlButton.replace(`qty=${getParameter('qty', urlButton)}`, `qty=${qty}`);
                        $this.attr('href', urlButton);

                        $.ajax({
                            url: urlButton,
                            beforeSend: function () {
                                $this.html(current.loader);
                            },
                            success: function () {
                                self.createListItems('open');
                            },
                            complete: () => {
                                $this.html(htmlDefault);
                            },
                            error: function () {
                                console.log('error');
                            }
                        });
                    }
                }
            });
        }

        // discountProgressive
        $(document).on('click', '.btn-discount-progressive', (e) => {
            let $btnCurrent = $(e.currentTarget), url = $btnCurrent.attr('href'),
                htmlDefault = $('.btn-discount-progressive').html();

            e.preventDefault();
            $.ajax({
                url: url,
                beforeSend: () => {
                    $btnCurrent.html(this._config.loader);
                },
                success: () => {
                    this.createListItems('open');
                },
                complete: function () {
                    $btnCurrent.html(htmlDefault);
                },
                error: function () {
                    console.log('error');
                }
            });
        });
    }
}

