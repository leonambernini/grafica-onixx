﻿import getVariables from './_variables.js'
const _variables = getVariables();

export default function carousel(element = '[data-admake="carousel"]') {
    var carousel = {..._variables.carousel};
    
    if( $.fn.slick && $(element).length ){
        $.each( $(element), function(){
            let $this = $(this), 
                $e = $this,
                breakpoints = ['1200', '992', '768', '576'],
                configs = {},
                finalConfig = {..._variables.carousel},
                dataQty = $this.attr('data-qty'),
                arrayQty = [],
                dataConfig = $this.attr('data-config'),
                dataSelector = $this.attr('data-selector'),
                responsive = [];

            if( $e.find('.helperComplement').length ){
                $e.find('.helperComplement').remove();
            }

            if( dataConfig !== undefined && dataConfig !== null && dataConfig !== '' ){
                dataConfig = dataConfig.replace(/\'/g, '"');
                configs = JSON.parse(dataConfig);
            }

            if( dataSelector !== undefined && dataSelector !== null && dataSelector !== '' ){
                $e = $this.find(dataSelector);
            }

            if( dataQty !== undefined && dataQty !== null && dataQty !== '' && dataQty.indexOf('-') > -1 ){
                arrayQty = dataQty.split('-');
                for( var x = 1; x < arrayQty.length; x++ ){
                    responsive.push({
                        breakpoint: breakpoints[x-1],
                        settings: {
                            slidesToShow: parseInt(arrayQty[x]),
                            slidesToScroll: parseInt(arrayQty[x])
                        }
                    });
                }
            }
            
            for( var config in configs ){
                finalConfig[config] = configs[config];
            }

            finalConfig['responsive'] = responsive;
            finalConfig['slidesToShow'] = parseInt(arrayQty[0] || dataQty);
            finalConfig['slidesToScroll'] = parseInt(arrayQty[0] || dataQty);

            $e.slick(finalConfig);
        });
    }
    
}