﻿const loader = '<i class="fas fa-sync fa-spin"></i>',
    prevArrow = `<button type="button" class="slick-prev btn"><svg xmlns="http://www.w3.org/2000/svg" width="11.377" height="21.886" viewBox="0 0 11.377 21.886"><path id="Path" d="M11.256.121a.422.422,0,0,0-.591,0L.121,10.666a.422.422,0,0,0,0,.591L10.666,21.8a.422.422,0,0,0,.591-.591L1.006,10.961,11.256.711A.422.422,0,0,0,11.256.121Z" transform="translate(0 0)" fill="#b9b9b9"/></svg></button>`,
    nextArrow = `<button type="button" class="slick-next btn"><svg xmlns="http://www.w3.org/2000/svg" width="11.377" height="21.886" viewBox="0 0 11.377 21.886"><path id="Path" d="M-11.256.121a.422.422,0,0,1,.591,0L-.121,10.666a.422.422,0,0,1,0,.591L-10.666,21.8a.422.422,0,0,1-.551-.039.422.422,0,0,1-.039-.551l10.25-10.25L-11.256.711A.422.422,0,0,1-11.256.121Z" transform="translate(11.376 0)" fill="#b9b9b9"/></svg></button>`;
const variables = {
    loader: loader,
    headers: {
        "Accept": "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json",
        "x-vtex-api-appKey": "vtexappkey-graficaonixx-YGAUXJ",
        "x-vtex-api-appToken": "BBBGLHYFEGIMCYEWEYQEOZPSSLMZTSTLAARVVWHJFRJABOUKWWSSOLPBMJOAVYLLMWMBHJAIQHYXQPQDQCDXABXOKPJMHHNGFXNFBSCMWKBHMSLXSKJTVOLLYXRRANAJ"
    },
    carousel: {
        dots: false,
        arrows: true,
        infinite: true,
        prevArrow: prevArrow,
        nextArrow: nextArrow
    },
    cart: {
        // structs
        elementMain: 'admake-cart-sidebar',
        title: 'Seu Carrinho',
        btnClose: '<i class="far fa-times-circle"></i>',
        loader: loader,
        selectorBadge: '.admake-btn-smart-cart .badge',
        btnBuyMore: '',
        btnGoCart: 'Finalizar Compra',
        keepBuying: 'Continuar Comprando',
        subTotal: true,
        total: false,
        discount: false,
        // alets: [],
        alerts: ['* Frete calculado na finalização do pedido.'],

        // items
        qtyItem: true,
        listPrice: true,
        bestPrice: true,
        btnRemove: '<i class="fas fa-trash-alt"></i>',

        // buy
        buyAsyn: true,
        elementBuy: '.buy-button-normal > a, #ad-product-buy-button .buy-button',
        elementQty: '.btn-add-remove',
        textBuyButton: 'Fazer Pedido'
    },
    showcaseDepartment: {
        // scroll
        infiniteScroll: false,
        btnLoadMore: true,
        titleBtnLoad: 'Carregar mais produtos',
        showcase: '.admake-showcase[id*=ResultItems]',
        loader: loader,

        // filter
        orderButtons: false,
        classesOrder: 'd-none d-md-block flex-wrap d-md-flex justify-content-between align-items-center text-center'
    },
    productImage: {
        carouselDesktop: false,
        mobile: true,
        thumbsQty: 5,
        thumbVertical: false,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
    },
    shipping: {
        loader: loader,
        calculateQty: true,
        titleValue: 'Valores',
        titleMsg: 'Opções de Entrega',
        resultInfo: 'Frete {S-NAME}, entrega em {S-DAYS} para o CEP {S-CEP}',
        btnCalc: '#btn-shipping',
        inputText: '#cep-shipping'
    },
    reviews: {
        column: false,
        pagination: false,
        qtyByPage: 1
    },
    navbar: {
        // struct
        version: 'manual', // (auto, manual)
        levels: 2, // ( > 2 somente na versao auto )
        fluid: false,
        subContainer: true,
        qtyNivel3: 5,
        showMore: 'ver mais',

        // all department - qtyMax (auto), orientation (vertical, horizontal)
        allDepartment: true,
        titleAll: "Todos os <br> Departamentos",
        qtyMax: 0,

        mobile: true,
        mobileElement: '#header-nav-mobile .nav-menu ul',
        mobileBtnElement: '#header-bar-menu',
        mobileType: 'dropdown'
    },
    discountProgressive: {
        idCalculator: '718ad4e6-e2b1-4456-8775-6e457b7db377',
        textQty: 'Cantitate',
        textDiscount: 'Discount',
        btnBuy: '<i class="fas fa-cart-plus"></i>'
    },
    estimateDate: {
        title: 'LA CERERE',
        text: 'Produs disponibil la cerere. Livrarea produsului se va face in cel mult 30 de zile de la plasarea comenzii.'
    }
}

export default function getVariables(param) {
    return (param !== undefined && param !== null && variables[param] !== undefined) ? variables[param] : variables;
}