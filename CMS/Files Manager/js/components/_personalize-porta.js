import getVariables from './_variables.js'
import { stringToSlug, formatMoney } from './_functions.js'


const headers = getVariables('headers');
export default class personalizePorta {
    constructor() {
        if( $('#admake-personalizar-porta').length ){
            let self = this;
    
            self.getAnexoByProduct();
            self.events();
        }
    }
    getAnexoByProduct(){
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/api/catalog_system/pub/products/search/?fq=productId:"+$('#___rc-p-id').val(),
            "method": "GET",
            "headers": {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        }
          
        $.ajax(settings).done(function (response) {
            console.log(response)
            if( response != undefined && response != null && response.length ){
                let assemblyOptions = response[0]['itemMetadata']['items'][0]['assemblyOptions'];

                for( let x = 0; x < assemblyOptions.length; x++ ){
                    if( assemblyOptions[x]['id'] == "Estampa" ){
                        let estampas = assemblyOptions[x]['inputValues']['Estampa']['domain'];
                        for( let y = 0; y < estampas.length; y++ ){
                            let url = '/arquivos/' + estampas[y] + '.png';
                            $('.get-estampas').append(`<button data-value="${estampas[y]}" class="personalize-estampa"><img src="${url}" alt="${estampas[y]}" width="40" height="40" class="estampa" /></button>`)
                        }
                        $('.get-estampas .img-loader').remove();
                        $('.get-estampas .personalize-estampa').eq(1).click();
                    }else if( assemblyOptions[x]['id'] == "Letra" ){
                        let letras = assemblyOptions[x]['inputValues']['Letra']['domain'];
                        for( let y = 0; y < letras.length; y++ ){
                            $('.get-letras').append(`<button data-value="${letras[y]}" class="personalize-letra">${letras[y]}</button>`)
                        }
                        $('.get-letras .img-loader').remove();
                    }else if( assemblyOptions[x]['id'] == "Nome" ){

                    }
                }
            }
        });
    }
    events() {
        const self = this;
        
        $(document).on('click', '.personalize-letra', function(){
            $('.get-letra').html($(this).attr('data-value'));
            $('.attachment-box input.attachment-value[data-id="Letra"]').val($(this).attr('data-value'));
            $('.personalize-letra').removeClass('selected');
            $(this).addClass('selected');
            return false;
        });

        $(document).on('click', '.personalize-estampa', function(){
            $('.personalize-estampa').removeClass('selected');
            $(this).addClass('selected');
            $('.attachment-box input.attachment-value[data-id="Estampa"]').val($(this).attr('data-value'));
            $('.get-result .get-letra, .get-result .get-nome').css({
                'background-image': 'url('+$(this).find('img').attr('src')+')'
            });
            return false;
        });
        
        $(document).on('click', '.personalize-name-apply', function(){
            $('.get-nome').html($('input[name="personalize-name"]').val());
            $('.attachment-box input.attachment-value[data-id="Nome"]').val($('input[name="personalize-name"]').val());
            return false;
        });

        $(document).on('click', '.add-to-cart-personalize', function(){
            if($('#ad-product-buy-button .buy-button').length){
                $('#ad-product-buy-button .buy-button').trigger('click');
            }
        });
        
    }
}

