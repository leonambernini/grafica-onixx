﻿import getVariables from '../components/_variables.js'
const _variables = getVariables();

export default (element = '#page-home') => {
    if ($(element).length) {

        if ($(window).outerWidth() < 768) {
            let finalConfig = {
                ..._variables.carousel, ...{
                    slidesToShow: 3,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    responsive: [
                        {
                            breakpoint: 576,
                            settings: {
                                slidesToShow: 2,
                                variableWidth: false
                            }
                        },
                    ]
                }
            };

            $('#home-brands .content').slick(finalConfig);
        }

    }
}