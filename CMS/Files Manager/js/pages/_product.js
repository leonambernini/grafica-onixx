import ProductImage from '../components/_product-image.js'
import Shipping from '../components/_shipping.js'
import Reviews from '../components/_reviews.js'
import addRemQty from '../components/_addRemQty.js'
import personalizePorta from '../components/_personalize-porta.js'
import personalizeBanner from '../components/_personalize-banner.js'
import { billetPrice, stringToSlug, formatMoneyCheckout, formatMoney, ADMAKE_ALERT } from '../components/_functions.js'

function showHideQty() {
    if ($('.product-buy .notifyme-form').length && $('.product-buy .notifyme-form').is(':visible')) {
        $('.product-buy').addClass('no-stock');
    } else {
        $('.product-buy').removeClass('no-stock');
    }
}

export default (element = '#page-product') => {

    if ($(element).length) {
        addRemQty();
        // titleContentExists();
        showHideQty();

        new ProductImage;
        new Shipping;
        new Reviews;
        new personalizePorta;
        new personalizeBanner;

        $('#cep-shipping').blur( function(){
            var $this = $(this);
            var cep = $(this).val();
            if( cep != undefined && cep != null && cep != '' && cep.length == 9 ){
                $.each( $('.seller-shipping'), function(){
                    var items = [{
                        id: vtxctx['skus'],
                        quantity: parseInt($('.quantitySelector input').val()),
                        seller: $(this).attr('data-seller-id')
                    }];
                    var postalCode = cep;
                    var country = 'BRA';

                    $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
                    vtexjs.checkout.simulateShipping(items, postalCode, country)
                        .done(function(result) {
                            console.log(result)
                            if( result['items'] != undefined && result['items'] != null && result['items'].length ){
                                var item = result['items'][0];
                                var logisticsInfo = result['logisticsInfo'][0]['slas'][0];
                                var $seller = $('.seller-shipping[data-seller-id="'+item['seller']+'"]');
                                if( $seller.length ){
                                    let days = parseInt(logisticsInfo.shippingEstimate);
                                    let textDays = days + ( ( days > 1 ) ? ' dias úteis' : ' dia útil' );
                                    $seller.html(
                                        '<table>'+
                                        '   <tr>' +
                                        '       <th>Entrega</th>' +
                                        '       <td>'+logisticsInfo['name']+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Frete</th>' +
                                        '       <td>R$ '+formatMoneyCheckout(logisticsInfo['price'], 2, ',', '.')+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Prazo</th>' +
                                        '       <td>'+textDays+'</td>' +
                                        '   </tr>' +
                                        '</table>'
                                    );
                                }
                            }
                        });
                });
            }
        });


        // $('#buy-box-float .col-photo').html(`<img src="${$('#image-main').attr('src')}" alt="" width="70" class="img-fluid" />`);
        // $('#buy-box-float .col-name').html(`<p>${$('.product-name > .productName').html()}<p/>`);
        // $('#buy-box-float .col-price').html($('.product-price-variation .product-price').html());
        
        $(window).scroll( function(){
            // if( $('.price-border .no-stock').length && $('#buy-box-float').is(':visble') ){
            //     $('#buy-box-float').fadeOut();
            // }else if( !$('.price-border .no-stock').length ){
            //     if($('.product-price-variation').length){
            //         if( $(this).scrollTop() >= $('.product-price-variation').offset().top && !$('#buy-box-float').is(':visible') ) {
            //             $('#buy-box-float').fadeIn();
            //         }else if( $(this).scrollTop() < $('.product-price-variation').offset().top && $('#buy-box-float').is(':visible') ) {
            //             $('#buy-box-float').fadeOut();
            //         }
            //     }
            // }

            if( $('.price-border .no-stock').length && $('#admake-buy-box-float').is(':visble') ){
                $('#admake-buy-box-float').fadeOut();
            }else if( !$('.price-border .no-stock').length ){
                if($('.product-price-variation').length){
                    if( $(this).scrollTop() >= $('.product-price-variation .col-buy-button').offset().top && !$('#admake-buy-box-float').is(':visible') ) {
                        $('#admake-buy-box-float').fadeIn();
                    }else if( $(this).scrollTop() < $('.product-price-variation .col-buy-button').offset().top && $('#admake-buy-box-float').is(':visible') ) {
                        $('#admake-buy-box-float').fadeOut();
                    }
                }
            }
        });


        if( $('#divCompreJunto table').length && $('.admake-buy-together').length ){
            $('.admake-buy-together').fadeIn();
        }

        // let discountBoleto = function(){
        //     if( $('.col-image .discount').length ) {
        //         let $flag = $('.col-image .discount').find('[class*="boleto"]'),
        //             $best = $('.descricao-preco .skuBestPrice'),
        //             $html = $('.plugin-preco .preco-a-vista');

        //         $('.descricao-preco .valor-dividido.price-installments').before('<div class="product-best-price-discount"></div><div class="best-price-msg">desconto à vista 1X cartão ou boleto</div>');
        //         $('.descricao-preco').after('<div class="new-sku-best-price"><span>Total a prazo </span>' + $best.html() + '</div>');

        //         $('.productPrice .preco-a-vista.price-cash').hide();
        //         $best.hide();
        //         billetPrice($flag, $best, $('.product-best-price-discount'), 1);

        //         $('.productPrice').addClass('active');
        //     };
        // }
        // discountBoleto();

        if ($('.quantitySelector > label > input').length) {
            $('.quantitySelector > label').append(`
                    <div class="actions">
                        <button class="add-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="add" tabindex="0">+</button>
                        <button class="remove-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="remove" tabindex="0">-</button>
                    </div>`);
        }

        // events
        $(document).on('change', '.sku-selector', function () {
            showHideQty();
            // discountBoleto();
        });

        if($('.cta-personalizar').length && $('#admake-personalizar-porta').length){
            $(document).on('click', '.cta-personalizar', function () {
                // var top = $('#admake-personalizar-porta').offset.top();
    
                // $(window).animation({
                //     scrollTop: top,
                // }, 300);

                $('html, body').animate({  
                    scrollTop: $('#admake-personalizar-porta').offset().top - 150
                }, 1500);
                
            });
        }
        
        if($('.simu-calculator').length && !$('.Tamanhoderolo .item-dimension-Tamanhoderolo label.dimension-Tamanhoderolo').length){
            $('.simu-calculator').hide();
        };
        
        //Calculadora m2
		function calculator(waste) {
            if($('.Tamanhoderolo .item-dimension-Tamanhoderolo label.dimension-Tamanhoderolo').text().split('x') != ""){
                var calculadoraPapel = $('.simu-calculator'),
                    resultado = calculadoraPapel.find('.result-calculator'),
                    medidas = $('.Tamanhoderolo .item-dimension-Tamanhoderolo label.dimension-Tamanhoderolo').text().split('x'),
                    altura  = parseFloat( medidas[0].replace(',', '.') ) || null,
                    largura = parseFloat( medidas[1].replace(',', '.') ) || null;

                var _altura  = calculadoraPapel.find('.main-calculator .height-simu input');
                var _largura = calculadoraPapel.find('.main-calculator .width-simu input');

                if( _largura.val() === null || _largura.val() === '' || _altura.val() === null || _altura.val() === '' ){
                    calculadoraPapel.addClass('error');
                    resultado.addClass('d-none');
                }else{
                    var _m2 = _altura.val().replace(',', '.') * _largura.val().replace(',', '.');
                    var m2 = altura * largura;

                    var waste = m2 * waste;

                    var qtdRolos = Math.ceil((_m2 / waste).toFixed(4));
                    //Precisei fazer isso por que segundo o cliente o cálculo estava retornando um resultado incorreto
                    //após um ajustes no parametro waste, começou a retornar q roloa a mais do que o correto
                    if(qtdRolos > 1){
                        // A quantidade mínima de rolo é 1
                        qtdRolos = qtdRolos - 1;
                    }

                    calculadoraPapel.removeClass('error');
                    resultado.removeClass('d-none');
                    resultado.find('.qty-calculator strong').html(qtdRolos);
                    
                    $('#page-product #ad-product-buy-button .quantitySelector input').val( qtdRolos );

                    if( qtdRolos == '1' ){
                        resultado.find('.qty-calculator').addClass('single');
                        resultado.find('.qty-calculator').removeClass('plural');
                    }else{
                        resultado.find('.qty-calculator').addClass('plural');
                        resultado.find('.qty-calculator').removeClass('single');
                    }
                }
            }else{
                alert('Por favor, selecione o tamanho do rolo');
            }
		}
        //end Calculadora m2
        
        if($('.simu-calculator').length){
            $('.simu-calculator .main-calculator > div input').mask('000.000.000.000.000,00', {reverse: true});

            var calculadoraPapel = $('.simu-calculator');
    
            if( calculadoraPapel.length ){
                // var type = $('.product-specification .value-field.Tipo').text();
                var type = 'Papel de Parede';
                
                if(type.indexOf('Faixa') == 0){
                    // var medidas = $('.product-specification .value-field.Tamanho-do-Rolo').text().split('x'),
                    var medidas = $('.Tamanhoderolo .item-dimension-Tamanhoderolo label.dimension-Tamanhoderolo').text().split('x'),
                        altura  = parseFloat( medidas[0].replace(',', '.') ) || null;
    
                    calculadoraPapel.find('.main-calculator .height-simu input').val(altura).prop('disabled', true);
                }
                
                $('.simu-calculator .main-calculator .btn-simu').on('click', function(event) {
                    event.preventDefault();
                    
                    calculator(0.95);

                    // if(type.indexOf('Faixa') == 0){
                    //     calculator(0.95);
                    // }else{
                    //     if(type.indexOf('Papel de Parede') == 0){
                    //         // calculator(0.8);
                    //         calculator(0.95);
                    //     }
                    // }
                });
            }
        }
    }
    
    if($('#page-product main .product-main .product-variations .topic .skuList span label[class*="Cor"]').length){
        $('#page-product main .product-main .product-variations .topic .skuList span label[class*="Cor"]').each(function(){
            $(this).css('background', `url(https://graficaonixx.vteximg.com.br/arquivos/${$(this).text()}.png)`);
        });
    }
    if($('#page-product main .product-main .product-variations .topic .skuList span label').length){
        $('#page-product main .product-main .product-variations .topic .skuList span label').on('click', function(){
            //Para manter selecionado a cor no popup de simula��o de Ambiente
            $('#page-product main .product-main .product-variations .topic .skuList span label').removeClass('active');
            $('#page-product main .product-main .product-variations .topic .skuList span label[for="'+$(this).attr('for')+'"]').addClass('active');
        });
    }

    function check_first_variations(){
        if($('.product-main-personalize-banner .box-variations-upload-personalize-banner ul .skuList span').length){
            if($('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul.box-custom-format .skuList span label').length){
                $('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul.box-custom-format .skuList span label:first-child').trigger('click');
            };
            $.each( $('.product-main-personalize-banner .box-variations-upload-personalize-banner ul .skuList span'), function(){
                var $skuSpan = $(this);
                $.each( $skuSpan.find('input[type="radio"]'), function(index){
                    var $skuInput = $(this);
                    if(index == 0){
                        $skuInput.trigger('click');
                    }
                });
            });
        }
    }

    function update_summary(){
        if($('.summary-personalize-banner').length){
            $('.summary-personalize-banner ul').empty();
            if($('.product-main-personalize-banner .box-variations-upload-personalize-banner .specification').length){
                $.each( $('.product-main-personalize-banner .box-variations-upload-personalize-banner .specification'), function(){
                    if($(this).next('.skuList').find('label:not(.label-custom-format).sku-picked').length){
                        $('.summary-personalize-banner ul').append('<li><span class="summary-label">'+$(this).text()+': </span><strong>'+$(this).next('.skuList').find('label.sku-picked').text()+'</strong></li>');
                    }else{
                        if($(this).next('.skuList').find('label.label-custom-format.sku-picked').length){
                            if($('.label-custom-format .custom-format').val() != ""){
                                $('.summary-personalize-banner ul').append('<li><span class="summary-label">'+$(this).text()+': </span><strong>'+$('.label-custom-format .custom-format').val()+'</strong></li>');
                            }
                            else{
                                $('.summary-personalize-banner ul').append('<li><span class="summary-label">'+$(this).text()+': </span><strong></strong></li>');
                            }
                        }else{
                            $('.summary-personalize-banner ul').append('<li><span class="summary-label">'+$(this).text()+': </span><strong></strong></li>');
                        }
                    }
                });                
            }
        };
    };

    if($('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul.box-custom-format .skuList span label').length){
        $('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul.box-custom-format .skuList span label').on('click', function(){
            $('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul.box-custom-format .skuList span label').removeClass('sku-picked');
            $(this).addClass('sku-picked');
            
            if($('.attachment-format').length){
                if(!$(this).hasClass('label-custom-format')){
                    $('.attachment-format').val($(this).text());
                    $('#page-product .product-main-personalize-banner .box-variations-upload-personalize-banner ul .skuList span input.custom-format').val('');
                }else{
                    $('.attachment-format').val($('.label-custom-format .custom-format').val());
                }
            }

            update_summary();
        });
    };

    if($('input.custom-format').length){
        $('input.custom-format').keyup(function(){
            $('.attachment-format').val($(this).val());
            update_summary();
        });
    };

    check_first_variations();
    update_summary();
    
    if($('.product-main-personalize-banner .box-variations-upload-personalize-banner ul .skuList span input[type="radio"]').length){
        $('.product-main-personalize-banner .box-variations-upload-personalize-banner ul .skuList span input[type="radio"]').change(function() {           
            update_summary();
        });
    }

    if($('#modal-simule-o-ambiente').length){
        if($('.cta-simule-o-ambiente').length){
            $('.cta-simule-o-ambiente, .cta-simule-o-ambiente-in-photo').on('click', function(){
                $('#modal-simule-o-ambiente').fadeIn();
            });
        };
        $('#modal-simule-o-ambiente .mask, #modal-simule-o-ambiente .close, #modal-simule-o-ambiente .cta-ok-simule-o-ambiente').on('click', function(){
            $('#modal-simule-o-ambiente').hide();
        });
    }else{
        $('.cta-simule-o-ambiente').hide();
    }

    if($('#modal-simule-o-ambiente .box-simule-o-ambiente .simule-o-ambiente .colors ul.colors-simulate li span').length){
        $('#modal-simule-o-ambiente .box-simule-o-ambiente .simule-o-ambiente .colors ul.colors-simulate li span').each(function(){
            $(this).css('background', `url(https://graficaonixx.vteximg.com.br/arquivos/${$(this).text()}.png)`);
        });
        $('#modal-simule-o-ambiente .box-simule-o-ambiente .simule-o-ambiente .colors ul.colors-simulate li').on('click', function(){
            $('#modal-simule-o-ambiente .box-simule-o-ambiente .simule-o-ambiente .colors ul.colors-simulate li').removeClass('active');
            $(this).addClass('active');
            $('#modal-simule-o-ambiente .box-simule-o-ambiente .simule-o-ambiente .image, #page-product main .product-main .product-image-flags .product-image #show #include').css('background', `url(https://graficaonixx.vteximg.com.br/arquivos/${$(this).find('span').text()}.png)`);
            $('.product-image-mobile').css('background', `url(https://graficaonixx.vteximg.com.br/arquivos/${$(this).find('span').text()}.png)`);
        })
    }

    if($('#page-product main .product-main .product-variations .topic .specification').length){
        $('#page-product main .product-main .product-variations .topic .specification').each(function(){
            if($(this).text() == "Cor do Adesivo"){
                $(this).text('Selecione a cor do adesivo');
            }
        });
    }
    if($('#page-product main .product-main .product-variations [class*="Cor"] .specification').length){
        $('#page-product main .product-main .product-variations [class*="Cor"] .specification').prepend('<img src="https://graficaonixx.vteximg.com.br/arquivos/color-icon.png" width="20" />');
    };

    if($('.box-summary-personalize-banner .close-summary').length){
        $('.box-summary-personalize-banner .close-summary').on('click', function(){
            $(this).parents('.box-summary-personalize-banner').toggleClass('visible');
        });
    };

    if($('.box-btn-summary').length){
        $('.box-btn-summary').on('click', function(){
            $('.box-summary-personalize-banner').toggleClass('visible');
            var $doc = $('html, body');
            $doc.animate({
                scrollTop: $('.box-summary-personalize-banner').offset().top
            }, 500);
        });
    };

    if($('#page-product main.product-main-personalize-banner .product-main .plugin-preco .valor-dividido > span').length){
        $('#page-product main.product-main-personalize-banner .product-main .plugin-preco .valor-dividido > span').append(' no cartão');
    };

    if(jQuery(".box-summary-personalize-banner .to-fix").length){
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 150) {
                jQuery(".box-summary-personalize-banner .to-fix").addClass("summary-fixed");
            } else {
                jQuery(".box-summary-personalize-banner .to-fix").removeClass("summary-fixed");
            }
        });
    };

    //Checagem profissional em Banner personalizado
    setTimeout(function(){
        if($('.checagem-profissional-value span strong').length && $('.box-acessories-price .acessories-price').length){
            $('.checagem-profissional-value span strong').text('+ '+$('.box-acessories-price .acessories-price').html());
        };
    }, 500);

    if($('.checagem-profissional-cta').length && $('.admake-acessories .buy-product-checkbox:not(.buy-product-checkbox-checked)').length){
        const $checkboxChecagem = $('.admake-acessories .buy-product-checkbox:not(.buy-product-checkbox-checked)');
        const $checkboxChecagemLabel = $checkboxChecagem.next('label');
        $('.checagem-profissional-cta label').attr('for',$checkboxChecagemLabel.attr('for'));

        $('.checagem-profissional-cta').on('click', function(){
            const $this = $(this);

            if($checkboxChecagem.is(':checked')){
                ADMAKE_ALERT(['Checagem profissional removida do banner']);
                $this.find('label').html('Adicionar ao banner');
            }else{
                ADMAKE_ALERT(['Checagem profissional adicionada ao banner']);
                $this.find('label').html('Remover do banner');
            };

            $('.box-upload.checagem-profissional').toggleClass('uploaded');
            $this.toggleClass('btn-remove-checagem');
        });
    };

    // if($('.admake-personalizar-impresao-banner').length){
    //     // Callback de seleção de variação
    //     $(document).ready(function () {
    //         var batchBuyListener = new Vtex.JSEvents.Listener('batchBuyListener', BatchBuy_OnSkuDataReceived);
    //         skuEventDispatcher.addListener(skuDataReceivedEventName, batchBuyListener);
    //     });

    //     function BatchBuy_OnSkuDataReceived(e) {
    //         var id = e.skuData.id;
    //         selectedToBuy = [];
    //         if (id > 0) {
    //             // console.log(e.skuData);
    //             // console.log('Haha');
                
    //             //R$ ${formatMoneyCheckout(element.listPrice, 2, ',', '.')}
    //         }
    //     };
    // };

    if($('.admake-header-prod-tabs ul li').length){
        $(document).on('click', '.admake-header-prod-tabs ul li', function () {
            let item_id = this.id;
            $('.admake-header-prod-tabs ul li').removeClass('active');
            $(this).addClass('active');
            $('.admake-product-tab-content').removeClass('active');
            $(`.admake-product-tab-content.${item_id}`).addClass('active');
        });
    };
    
    //Adicionando link para baixar gabarito na p�gina personalizada de Catálogo segundo o link que foi configurado no painel
    if($('.catalog-custom-cta-download .link-baixar-gabarito').length){
        var $link_gabarito = $('.catalog-custom-cta-download .link-baixar-gabarito');
        $link_gabarito.next('a').attr('href',$link_gabarito.find('span').text());
        $('a.link-img-download').attr('href',$link_gabarito.find('span').text());
    };
}