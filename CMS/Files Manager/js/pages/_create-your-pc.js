import { formatMoney } from '../components/_functions'
import getVariables from '../components/_variables.js'

const headers = getVariables('headers');

let options;
let activeCollection;
let cacheCollection = {};
let percent = 5;
let platform_type;

let productsCookie = {};

let myLStorege;

let htmlSteps = '';

let sendListFlag = false;

let htmlItem = `
    <div class="option-item {{ACTIVE}}" style="display: none;" data-collection="{{COLECTION}}" data-id="{{ID}}">
        <div class="d-flex justify-content-between align-items-center">
            <div class="option-item-icon">
                <img src="/arquivos/{{ICON}}" alt="{{TITLE}}" width="{{ICON_W}}" height="{{ICON_H}}" class="img-fluid" />
            </div>
            <div class="option-item-title mr-auto">
                <h3>{{TITLE}} {{REQUIRED}}</h3>
            </div>
            {{ITEM}}
            <div class="option-item-action d-flex">
                <button class="option-item-select" data-opcional="{{IS_REQUIRED}}" data-collection="{{COLECTION}}" data-id="{{ID}}"><span>Selecionar</span> <i class="far fa-edit"></i></button>
                <button class="option-item-remove" data-id="{{ID}}"><i class="fas fa-trash-alt"></i></button>
            </div>
        </div>
    </div>
`;

export default (element = '#page-create-you-pc') => {

    function lStorage(method, value){
        if( method == 'get' ){
            let createYourPcItem = localStorage.getItem('create-your-pc-items-'+platform_type);
            if( createYourPcItem == undefined || createYourPcItem == null ){
                lStorage('set', {'items': {}});
                return {'items': {}};
            }
            myLStorege = JSON.parse(createYourPcItem);
            return JSON.parse(createYourPcItem);
        }else if( method == 'set'){
            myLStorege = value;
            localStorage.setItem('create-your-pc-items-'+platform_type, JSON.stringify(value));
        }
        calculatePrice();
    }

    function __init($items){
        let html = '';
        let activeIsTrue = false;
        let classActive = 'active';
        
        for( let x = 0; x < options.length; x++ ){
            let item = options[x];
            let htmlProduct = '';

            if( myLStorege['items'][item['id']] != undefined  && myLStorege['items'][item['id']] != null ){
                classActive = 'is-ok';
                let prodName = myLStorege['items'][item['id']]['prodName'];
                let prodImage = myLStorege['items'][item['id']]['prodImage'];
                if( prodName != 'not-product' ){
                    htmlProduct = `
                        <div class="product-image"><img src="${prodImage}" alt="" width="60" height="60" class="img-fluid" /></div>
                        <div class="product-name">${prodName}</div>
                    `;
                }
            }


            htmlSteps += `
                <div class="cyp-step-item" data-id="${item['id']}" data-colecao="${item['colecao']}" data-titulo="${item['titulo']}" data-is-required="${item['opcional']}">
                    <img src="/arquivos/${item['icone']}"alt="${item['titulo']}" width="${parseInt(item['icone_width'])-10}" height="${parseInt(item['icone_height'])-10}" class="img-fluid" />
                </div>
            `;

            if( classActive == 'active' ){
                if( activeIsTrue ){
                    classActive = '';
                }else{
                    activeIsTrue = true;
                }
            }

            html += htmlItem
                        .replace(/{{ACTIVE}}/g, classActive)
                        .replace(/{{ID}}/g, item['id'])
                        .replace(/{{COLECTION}}/g, item['colecao'])
                        .replace(/{{ICON}}/g, item['icone'])
                        .replace(/{{ICON_W}}/g, item['icone_width'])
                        .replace(/{{ICON_H}}/g, item['icone_height'])
                        .replace(/{{TITLE}}/g, item['titulo'])
                        .replace(/{{REQUIRED}}/g, (item['opcional']) ? '<small>(Opcional)</small>' : '')
                        .replace(/{{ITEM}}/g, htmlProduct)
                        .replace(/{{IS_REQUIRED}}/g, item['opcional']);

            if( classActive == 'is-ok' ){
                classActive = 'active';
            }else{
                classActive = '';
            }
        }

        $items.html(html);
        $('.option-item').fadeIn();
    }

    function setActiveClassInBox(){
        let classActive = false;
        $('.option-item').removeClass('active');
        $.each( $('.option-item'), function(){
            if( !$(this).hasClass('is-ok') && !classActive ){
                $(this).addClass("active");
                classActive = true;
            }
            // .next('.option-item:not(.is-ok)').addClass('active');
        })
    }

    function modal(method){
        let $modal = $('#create-your-pc-modal');

        if( !$modal.length ){
            createModal();
        }
        
        if( method == 'open' ){
            $modal.fadeIn();
        }else{
            $('.cyp-modal-body').html('<i class="fas fa-circle-notch fa-spin"></i>');
            calculatePrice();
            $modal.fadeOut();
        }
    }

    function calculatePrice(){
        let totalPrice = 0;

        for( let item in myLStorege['items'] ){
            let bestPrice = myLStorege['items'][item]['bestPrice'];
            totalPrice += ( bestPrice == undefined || bestPrice == null) ? 0 : bestPrice;
        }

        $('.cyp-get-price').html('R$ ' + formatMoney(totalPrice, 2, ',', '.'));
        if( totalPrice > 0 ){
            let boleto = ((100 - percent) * totalPrice) / 100;
            $('.cyp-get-price-boleto').html(`R$ ${formatMoney(boleto, 2, ',', '.')}`);
            $('.cyp-get-installment-price').html(`ou <strong>10x</strong> de <strong>R$ ${formatMoney(totalPrice/10, 2, ',', '.')}</strong> sem juros`);
        }else{
            $('.cyp-get-installment-price, .cyp-get-price-boleto').html('');
        }
    }

    function getTempItem(){
        let $product = $('.admake-create-pc-product.active');
        
        let productId = 'not-product';
        let productName = '';
        let productImage = '';
        let productOldPrice = 0;
        let productBestPrice = 0;
        let productHasCoollerBox = '';
        let refId = '';
        let productUrl = '';

        
        if( !$product.hasClass('not-product') ){
            productId = $product.attr('data-id');
            productName = $product.find('.product-name').text();
            productImage = $product.find('.product-image img').attr('src');
            productOldPrice = $product.find('.old-price').text();
            productBestPrice = $product.find('.best-price strong').text();
            productUrl = $product.find('.product-show-details a').attr('href');
    
            refId = parseInt($product.find('.is-checklist-item.first input.insert-sku-checkbox').attr('rel'));

            productOldPrice = parseFloat($.trim(productOldPrice.replace('R$', '').replace('.', '').replace(',', '.')));
            productBestPrice = parseFloat($.trim(productBestPrice.replace('R$', '').replace('.', '').replace(',', '.')));

            if( productsCookie[productId] != undefined && productsCookie[productId] != null ){
                if( productsCookie[productId]['Coolerbox'] != undefined && productsCookie[productId]['Coolerbox'] != null && productsCookie[productId]['Coolerbox'].length ){
                    productHasCoollerBox = productsCookie[productId]['Coolerbox'][0]
                }         
            }
        }

        
        let obj = {
            'titulo': activeCollection['titulo'],
            'opcional': activeCollection['opcional'],
            'prodId': productId,
            'prodName': productName,
            'prodImage': productImage,
            'oldPrice': productOldPrice,
            'bestPrice': productBestPrice,
            'collerBox': productHasCoollerBox,
            'prodRef': refId,
            'productUrl': productUrl
        };

        myLStorege['items'][activeCollection['id']] = obj

        sendListFlag = false;

        calculatePrice();
        return obj;
    }

    function actions(){
        $(document).on('click', '#create-your-pc-modal .close-modal, #create-your-pc-modal .btn-cancel', function(){
            lStorage('get');
            modal('close');
            return false;
        });

        $(document).on('click', '.option-item-remove', function(){
            let $box = $(this).parents('.option-item');
            let typeId = $(this).attr('data-id');

            delete myLStorege['items'][typeId]
            lStorage('set', myLStorege);
            
            $box.removeClass('is-ok');
            $box.find('.product-image').remove();
            $box.find('.product-name').remove();
            setActiveClassInBox();
            return false;
        });

        $(document).on('click', '.option-item-select', function(){
            let collection = $(this).attr('data-collection');
            let typeId = $(this).attr('data-id');

            modal('open');

            $('.cyp-step-item').removeClass('active');
            function searchType(type){
                return type['id'] == typeId
            }
            activeCollection = options.find(searchType);
            getColletion(collection, activeCollection['opcional']);

            $('.cyp-modal-title').html(activeCollection['titulo']);
            $('.cyp-step-item[data-id="'+activeCollection['id']+'"]').addClass('active');

            if( activeCollection['opcional'] ){
                $('#create-your-pc-modal').addClass('not-required');
            }else{
                $('#create-your-pc-modal').removeClass('not-required');
            }

            $('.cyp-modal-footer .msg-alert').remove();
            if( typeId == 'hd' || typeId == 'ssd' || typeId == 'fonte' ){
                let msg;
                if( typeId == 'fonte' ){
                    msg = 'Fonte Recomendada: <strong class="get-font-w">400w</strong> ou superior';
                }else{
                    msg = '<i class="fas fa-exclamation-triangle"></i> <b>IMPORTANTE</b> É necessário a escolha de HD e/ou SSD para poder montar seu PC';
                }
                $('.cyp-modal-footer').prepend('<div class="msg-alert">'+msg+'</div>');
            }

            calculatePrice();
            return false;
        });

        $(document).on('click', '.admake-create-pc-product', function(){
            let $this = $(this);
            let id = $this.attr('data-id');
            
            if( id != undefined && id != null && ( productsCookie[id] == undefined || productsCookie[id] == null ) ){
                getProduct(id);
            }

            setMesages($this, id);

            $('.admake-create-pc-product').removeClass('active');
            $this.addClass("active");

            getTempItem();
        });

        $(document).on('click', '#create-your-pc-modal .btn-apply', function(){
            let $activeBox = $('.option-item[data-id="'+activeCollection['id']+'"]');
            
            let obj = getTempItem();
            
            if( $activeBox.find('.product-image').length ){
                $('.product-image, .product-name', $activeBox).remove();
            }

            if( obj['prodId'] != 'not-product' ){
                $activeBox.attr('data-old-price', ( isNaN(obj.productOldPrice) ) ? 0 : obj.productOldPrice);
                $activeBox.attr('data-best-price', ( isNaN(obj.productBestPrice) ) ? 0 : obj.productBestPrice);
                $activeBox.attr('data-prod-id', obj.productId);
    
                let $actionBox = $('.option-item-action', $activeBox);
                
                $actionBox.before(`
                    <div class="product-image"><img src="${obj.prodImage}" alt="" width="60" height="60" class="img-fluid" /></div>
                    <div class="product-name">${obj.prodName}</div>
                `);
            }
            
            $activeBox.removeClass('active').addClass('is-ok');
            
            setActiveClassInBox();

            
            lStorage('set', myLStorege);
            modal('close');

            return false;
        });

        $(document).on('click', '#btn-send-list', function(){
            
            $('#send-list-modal .list').html('<i class="fas fa-circle-notch fa-spin"></i>');
            $('#send-list-modal').fadeIn(function(){
                let html = '';

                for( let item in myLStorege['items'] ){
                    let aux = myLStorege['items'][item];

                    if( aux['prodId'] != "not-product" ){
                        html += `
                            <div class="send-list-item" style="padding: 8px 30px;">
                                <a href="${(aux['productUrl']!=undefined)?aux['productUrl']:'#'}" title="${aux['prodName']}" style="display: block;">
                                    <img src="${aux['prodImage']}" alt="${aux['prodName']}" width="60" height="60" style="display: inline-block; vertical-align: middle; margin-right: 15px"/>
                                    <span class="title" style="display: inline-block; vertical-align: middle; font-weight: 400; font-size: 12px; color: #5C7189;">${aux['prodName']}<span>
                                </a>
                            </div>
                        `;
                    } 
                }

                $('#send-list-modal .list').html(html);
            });

            return false;
        });
        $(document).on('click', '#send-list-modal .close-modal', function(){
            $('#send-list-modal').fadeOut(function(){
                $('#send-list-modal .list').html('<i class="fas fa-circle-notch fa-spin"></i>');
            });
            return false;
        });

        $(document).on('click', '#btn-add-all-cart', function(){
            let text = $(this).text();
            $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
            if( $('.option-item ').not('.is-ok').find('.option-item-select[data-opcional="false"]').length ){
                alert('Para finalizar é necessario adicionar todos os itens não opcionais.');
                $(this).html(text);
                return false;
            }

            let items = [];

            for( let item in myLStorege['items'] ){
                let id = myLStorege['items'][item]['prodId'];
                let refId = myLStorege['items'][item]['prodRef'];
                if( id != 'not-product' ){
                    items.push({
                        id: parseInt(refId),
                        quantity: 1,
                        seller: '1'
                    })
                }
            }

            vtexjs.checkout.addToCart(items, null, 1).done(function(orderForm) {
                document.location.href = '/checkout/#/cart';
            });

            return false;
        })

        $(document).on('click', '#btn-send-list-to-email', function(){
            let email = $('#send-list-email').val();

            if( email == undefined || email == null || email == '' ){
                alert('Preencha seu e-mail');
                return false;
            }

            sendList(email, $('#send-list-modal .list').html(), $(this));
            return false;
        })
    }

    function sendList(email, list, $btn){
        if( !sendListFlag ){
            sendListFlag = true;
            let data = {
                "email": email,
                "list": list,
                "date": new Date()
            }

            var settings = {
                "async": true,
                "url": "/api/dataentities/MP/documents",
                "method": "POST",
                "type": "POST",
                "headers": headers,
                "processData": false,
                "beforeSend": function(){
                    $btn.html('<i class="fas fa-circle-notch fa-spin"></i>');
                },
                "data": JSON.stringify(data)
            }

            $.ajax(settings).done(function (response) {
                $btn.html('ENVIADO <i class="fas fa-check-double"></i>');
            });
        }
    }

    function createModal(){
        let htmlModalEmail = `
            <div id="send-list-modal" style="display: none;">
                <div class="send-list-mask close-modal"></div>
                <div class="send-list-content">
                    <div class="send-list-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h2 class="send-list-title">Enviar lista por e-mail</h2>
                            <button class="close-modal"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="send-list-body">
                        <h3>Confira abaixo todos os itens que você escolheu</h3>
                        <div class="list">
                            <i class="fas fa-circle-notch fa-spin"></i>
                        </div>
                    </div>
                    <div class="send-list-footer">
                        <div class="d-flex justify-content-between align-items-center">
                            <input type="email" name="send-list-email" id="send-list-email" class="flex-grow-1" placeholder="E-mail" />
                            <button id="btn-send-list-to-email">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        `;

        let htmlModal = `
            <div id="create-your-pc-modal" style="display: none;">
                <div class="cyp-mask close-modal"></div>
                <div class="cyp-modal-content">
                    <div class="cyp-modal-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h2 class="cyp-modal-title"><i class="fas fa-circle-notch fa-spin"></i></h2>
                            <button class="close-modal"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="cpy-modal-stages d-flex justify-content-between align-items-center">${htmlSteps}</div>
                    </div>
                    <div class="cyp-modal-body">
                        <i class="fas fa-circle-notch fa-spin"></i>
                    </div>
                    <div class="cyp-modal-footer">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="cyp-modal-price">
                                <b>TOTAL</b>
                                <strong class="cyp-get-price">
                                    R$ 0,00
                                </strong>
                                <span class="cyp-get-installment-price"></span>
                            </div>
                            <div class="cyp-modal-actions">
                                <button class="btn-cancel">Cancelar</button>
                                <button class="btn-apply">Confirmar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        if( !$('#create-your-pc-modal').length ){
            $('body').append(htmlModal + htmlModalEmail);
        }
    }

    function getColletion(colletion, opcional) {

        if( cacheCollection[colletion] != undefined && cacheCollection[colletion] != null ){
            $('#create-your-pc-modal .cyp-modal-body').html(cacheCollection[colletion]);
            return false;
        }

        $.ajax({
            url: '/buscapagina?fq=H:' + colletion + '&sl=33ea716a-7ad2-466e-8697-d1fb0b790331&PS=50&cc=50&sm=0&O=OrderByTopSaleDESC',
            method: "GET",
            beforeSend: function(){
                $('#create-your-pc-modal .cyp-modal-body').html('<i class="fas fa-circle-notch fa-spin"></i>');
            }
        }).done(function (html) {
            var $content = $($.parseHTML(html));
            var $showcase = $content.filter('.admake-showcase-create-pc')

            if( opcional ){
                $showcase.prepend(`
                    <div class="admake-create-pc-product not-product">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="product-checkbox"><i class="far fa-square"></i></div>
                            <div class="product-image"><i class="fas fa-ban"></i></div>
                            <div class="product-name flex-grow-1">Não Selecionar Nenhum ${activeCollection['titulo']}</div>
                        </div>
                    </div>
                `)
            }
            $('#create-your-pc-modal .cyp-modal-body').html($showcase);
    
            if( myLStorege['items'][activeCollection['id']] != undefined && myLStorege['items'][activeCollection['id']] != null ){
                let prodId = myLStorege['items'][activeCollection['id']]['prodId'];
                let $selectedItem = $('.admake-showcase-create-pc .admake-create-pc-product[data-id="'+prodId+'"]');

                if( $selectedItem.length ){
                    $selectedItem.click();
                }
            }

            cacheCollection[colletion] = $showcase;
        });
    }

    function setMesages($e, id){
        let msg;
        if( activeCollection['id'] == 'cooler' && $e.hasClass('not-product')){
            if( myLStorege['items']['processador'] != undefined && myLStorege['items']['processador'] != null && myLStorege['items']['processador']['collerBox'] != undefined && myLStorege['items']['processador']['collerBox'] == 'Não' ){
                msg = '<i class="fas fa-exclamation-triangle"></i> <b>Recomendamos</b> a escolha de um cooler, pois o processador escolhido não acompanha Cooler Box';
                $e.prepend('<div class="msg-alert">'+msg+'</div>');
            }
        }else if( activeCollection['id'] == 'fonte' ){
            if( $('.get-font-w').length && productsCookie[id] != undefined && productsCookie[id] != null && productsCookie[id]['Fonte'] != undefined && productsCookie[id]['Fonte'] != null && productsCookie[id]['Fonte'].length ){
                $('.get-font-w').html(productsCookie[id]['Fonte'][0])
            }
        }
    }

    function getProduct(id){

        if( activeCollection['id'] != 'processador' && activeCollection['id'] != 'fonte' ){
            return false;
        }
        var settings = {
            "async": false,
            "url": "/api/catalog_system/pub/products/search?fq=productId:"+id,
            "method": "GET",
            "headers": {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        }
          
        $.ajax(settings).done(function (response) {
            if( response.length ){
                productsCookie[id] = response[0];
            }
        });
    }

    if ($(element).length) {

        platform_type = ( $('#page-create-you-pc').hasClass('intel') ) ? 'intel' : 'amd';

        options = MONTE_SEU_PC[platform_type];
        let $items = $('#admake-monte-seu-pc-plataforma .col-items');
        
        lStorage('get');

        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            console.log('ADMAKE -orderForm init');
        });

        __init($items);
        createModal();
        actions();
        calculatePrice();
    }
}