﻿export default (element = '#page-account') => {
    if ($(element).length) {
        $('.modal').each(function () {
            $(this).find('> div, > form').wrapAll(`<div class="content" />`);
            $(this).find('.modal-header').append($(this).find('.modal-header .close'));
            $(this).prepend(`<div class="mask"></div>`);
        })

        $('.modal').removeClass('modal hide fade').addClass('modal-account');
        $('.modal-footer .btn-link').addClass('btn btn-secondary close');

        $('.modal-account .row [class*=span]:not(h5)').each(function () {
            $(this).addClass('col-6');
        });

        /** texts */
        
        /** events */
        $(document).on('click', '.modal-account .close', function () {
            $(this).parents('.modal-account').hide();
        });
        $(document).on('click', '[data-toggle="modal"]', function () {
            $($(this).attr('href')).show();
        });
    }
}